<?php

/**
 * APCManagement
 *
 * This php class is to work with alternative PHP cache (APC).
 *
 * @package APCManagement
 * @copyright Copyright (c) 2012-2013 Benjamin Butschko. (http://www.benjamin-butschko.de)
 * @license http://benjamin-butschko.de/license/new-bsd.html New BSD License
 * @version 2.0.0
 */

class APC_Management
{

	public $data;
	public $cache_key_name;
	public $lifetime                = 300;
	public $automatic_serialization = true;
	public $step_value              = 1;
	public $mode                    = 'user';

	/**
	 *
	 * This function / construct returns exception if apc-extension is not loaded
	 *
	 */
	 public function __construct()
	 {

		try
		{

			if (extension_loaded('apc') === false)
			{

				throw new Exception('FATAL ERROR! APCManagement caught exception: "The apc extension must be loaded for using this backend!"');

			}

		}
		catch(Exception $e)
		{

			echo $e->getMessage();
			die();

		}

    }

	// *****************************************************************************************************************************

	/**
	 *
	 * This function returns serialized data
	 *
	 * @param string $this->data data to serialize
	 *
	 * @return serialized data
	 *
	 */
	private function serialize_data()
	{

		try
		{

			if (empty($this->data) === true)
			{

				throw new Exception('WARNING! APCManagement::serialize_data() caught exception: "Cannot serialize data. Empty data given!"');

			}
			else
			{

				$this->data = serialize($this->data);
				return true;

			}

		}
		catch(Exception $e)
		{

			echo $e->getMessage();
			return false;
		}

	}

	// *****************************************************************************************************************************

	/**
	 *
	 * This function returns unserialized data
	 *
	 * @param string $this->data data to unserialize
	 *
	 * @return unserialized data or false
	 *
	 * Notice: Beware trying to unserialize data arrays instead of data strings
	 *
	 */

	private function unserialize_data()
	{

		try
		{

			if (empty($this->data) === true)
			{

				throw new Exception('WARNING! APCManagement::unserialize_data() caught exception: "Cannot unserialize data. Empty data given!"');

			}
			else if (is_string($this->data) === false)
			{

				throw new Exception('WARNING! APCManagement::unserialize_data() caught exception: "Cannot unserialize data. Expects data to be string, array given!"');

			}
			else
			{

				$this->data = unserialize($this->data);
				return true;

			}

		} catch(Exception $e) {

			echo $e->getMessage();
			return false;
		}

	}

	// *****************************************************************************************************************************

	/**
	 *
	 * This function check if cached data by cache keyname exists
	 *
	 * @param string $cache_key_name Name of cache object; Return true or false
	 *
	 * @return boolean true|false
	 *
	 */

	public function check_cache()
	{

		if (function_exists('apc_exists'))
		{

			if (apc_exists($this->cache_key_name) === true)
			{

				return true;

			}
			else{

				return false;

			}

		}
		else
		{

			if (apc_fetch($this->cache_key_name) === true)
			{

				return true;

			}
			else
			{

				return false;

			}

		}

	}

	// *****************************************************************************************************************************

	/**
	 *
	 * This function store data if cache object by name exists
	 *
	 * @param string $cache_key_name Name of cache object where data should be stored
	 * @param string $this->data Data can be a string or array
	 * @param int $lifetime Lifetime of cached data in seconds - Cached data will automaticly removed from apc cache handling; default: 60
	 * @param boolean $automatic_serialization Turn automatic serialization for data on or off (true or false); default: true
	 *
	 * @return boolean true|false
	 *
	 */
	public function store_data()
	{

		try
		{

			if (empty($this->cache_key_name) === true)
			{

				throw new Exception('WARNING! APCManagement::store_data() caught exception: "Cannot store data in cache. Empty Keyname given!"');

			}
			else if (empty($this->data) === true)
			{

				throw new Exception('WARNING! APCManagement::store_data() caught exception: "Cannot store data in cache. Empty data given!"');

			} else {

				if($this->automatic_serialization === true)
				{

					$this->serialize_data();

				}

				apc_store($this->cache_key_name, $this->data, $this->lifetime);
				return true;

			}

		}
		catch(Exception $e)
		{

			echo $e->getMessage();
			return false;

		}

	}

	// *****************************************************************************************************************************

	/**
	 *
	 * This function read data if cache object by name exists
	 *
	 * @param string $cache_key_name Name of cache object where data should be fetched
	 * @param boolean $automatic_serialization Turn automatic deserialization for data on or off (true or false); default: true
	 *
	 * @return object
	 *
	 */

	public function read_data()
	{

		try
		{

			if (empty($this->cache_key_name))
			{

				throw new Exception('WARNING! APCManagement::read_data() caught exception: "Cannot read data from cache. Empty keyname given!"');

			}
			else if ($this->check_cache($this->cache_key_name) === false)
			{

				throw new Exception('WARNING! APCManagement::read_data() caught exception: "Cannot read data from cache. Keyname "' . $this->cache_key_name . '" does not exists!"');

			}
			else
			{

				$this->data = apc_fetch($this->cache_key_name);

				if($this->automatic_serialization == true)
				{

					$this->unserialize_data();

				}

				return $this->data;

			}

		}
		catch(Exception $e)
		{

			echo $e->getMessage();
			return false;

		}

	}

	// *****************************************************************************************************************************

	/**
	 *
	 * This function delete data if cache object by name exists
	 *
	 * @param string $cache_key_name				>> Name of Cache object where data should be stored
	 *
	 * @return boolean true|false
	 *
	 */

	public function delete_data()
	{

		try
		{

			if (empty($this->cache_key_name) === true)
			{

				throw new Exception('WARNING! APCManagement::delete_data() caught exception: "Cannot delete data from cache. Empty keyname given!"');

			}
			else if ($this->check_cache($this->cache_key_name) === false)
			{

				throw new Exception('WARNING! APCManagement::delete_data() caught exception: "Cannot delete data from cache. Keyname "' . $this->cache_key_name . '" does not exists!"');

			}
			else
			{

				apc_delete($this->cache_key_name);
				return true;

			}

		}
		catch(Exception $e)
		{

			echo $e->getMessage();
			return false;

		}

	}

	// *****************************************************************************************************************************

	/**
	 *
	 * This function increase a stored number data if cache object by name exists
	 *
	 * @param string $cache_key_name	>> Name of Cacheobject where data should be stored
	 * @param int $step_value		>> Step / Value to increase
	 *
	 * @return boolean true|false
	 *
	 * IMPORTANT NOTE
	 * MAKE SURE THAT TO STORE AN CACHE OBJECT BEFORE DECREASE VALUES.
	 * DO NOT STORE SERIALIZED VALUES FOR USING THIS METHOD. TURN OFF AUTOSERIALIZATION INSTEAD.
	 * IN CONCLUSION USE read_data() METHOD BY USING OPTIONALLY PARAMETER TO TURN OFF AUTODESERIALIZATION.
	 *
	 */

	public function increase_number()
	{

		$this->automatic_serialization = false;

		try
		{

			if (empty($this->cache_key_name) === true)
			{

				throw new Exception('WARNING! APCManagement::increase_number() caught exception: "Cannot increase data from cache. Empty keyname given!"');

			}
			else if ($this->check_cache($this->cache_key_name) === false)
			{

				throw new Exception('WARNING! APCManagement::increase_number() caught exception: "Cannot delete data from cache. Keyname "' . $this->cache_key_name . '" does not exists!"');

			}
			else if (empty($this->step_value) === true)
			{

				throw new Exception('WARNING! APCManagement::increase_number() caught exception: "Cannot increase Value in Key "' . $this->cache_key_name . '". Empty increase value given!"');

			}
			else if (is_int($this->step_value) === false)
			{

				throw new Exception('WARNING! APCManagement::increase_number() caught exception: "Cannot increase Value in Key "' . $this->cache_key_name . '". Given Value is not an integer!"');

			}
			else
			{

				apc_inc($this->cache_key_name, $this->step_value);
				return true;

			}

		}
		catch(Exception $e)
		{

			echo $e->getMessage();
			return false;

		}

	}

	// *****************************************************************************************************************************

	/**
	 *
	 * This function decrease a stored number data if cache object by name exists
	 *
	 * @return boolean true|false
	 *
	 * IMPORTANT NOTE
	 * MAKE SURE THAT TO STORE AN CACHE OBJECT BEFORE DECREASE VALUES.
	 * DO NOT STORE SERIALIZED VALUES FOR USING THIS METHOD. TURN OFF AUTOSERIALIZATION INSTEAD.
	 * IN CONCLUSION USE read_data() METHOD BY USING OPTIONALLY PARAMETER TO TURN OFF AUTODESERIALIZATION.
	 *
	 */

	public function decrease_number()
	{

		$this->automatic_serialization = false;

		try
		{

			if (empty($this->cache_key_name) === true)
			{

				throw new Exception('WARNING! APCManagement::decrease_number() caught exception: "Cannot decrease data from cache. Empty keyname given!"');

			}
			else if ($this->check_cache($this->cache_key_name) === false)
			{

				throw new Exception('WARNING! APCManagement::decrease_number() caught exception: "Cannot delete data from cache. Keyname "' . $this->cache_key_name . '" does not exists!"');

			}
			else if (empty($this->step_value) === true)
			{

				throw new Exception('WARNING! APCManagement::decrease_number() caught exception: "Cannot decrease Value in Key "' . $this->cache_key_name . '". Empty increase value given!"');

			}
			else if (is_int($this->step_value) === false)
			{

				throw new Exception('WARNING! APCManagement::decrease_number() caught exception: "Cannot decrease Value in Key "' . $this->cache_key_name . '". Given Value is not an integer!"');

			}
			else
			{

				apc_dec($this->cache_key_name, $this->step_value);
				return true;

			}

		}
		catch(Exception $e)
		{

			echo $e->getMessage();
			return false;

		}

	}

	// *****************************************************************************************************************************

	/**
	 *
	 * This function returns cache information
	 *
	 * @param string $mode				>> return specifizied cache data; default: 'user'
	 *
	 * @return apc info
	 *
	 */
	public function info()
	{

		if ($this->mode == 'user')
		{

			$prepareCacheData = apc_cache_info('user');
			$cacheData        = $prepareCacheData['cache_list'];

		}
		else
		{

			$cacheData = apc_cache_info();

		}

		return (object) $cacheData;

	}

	// *****************************************************************************************************************************

}